package edu.bu.ec504.spr24.sameGameTris;

import java.io.Serial;
import java.util.EventObject;

/*
 * An event that is broadcast by one button to other buttons when the mouse rolls over it
 */
class CircleRolloverEvent extends EventObject {

	// Fields
	@Serial
	private static final long serialVersionUID = 1L;
	/*
	 * the location of the button broadcasting the event
	 */
	private final int _xx;
	private final int _yy;
	private final CircleColor clr;
	private final SelfAwareCircle.Visibility visibility;

	// Constructor(s)
	/*
	 * Records a rollover event on a specific button
	 * @param source The "source" upon which the event initially occurred
	 * @param xx The x coordinate (in the gridlayout of buttons) of the button that was rolled over
	 * @param yy The y coordinate (in the gridlayout of buttons) of the button that was rolled over
	 * @param clr The color of the button that initiated the event
	 * @param vis Should circles be highlighted or unhighlighted
	 */
	public CircleRolloverEvent(Object source, int xx, int yy, CircleColor clr,
			SelfAwareCircle.Visibility vis) {
		super(source); // register with the superclass
		this._xx = xx;
		this._yy = yy;
		this.clr = clr;
		this.visibility = vis;
	}

	// ACCESSORS
	int getXX() {return _xx;}

	int getYY() {return _yy;}

	SelfAwareCircle.Visibility getVisibility() {return visibility;}

	CircleColor getColor() {return clr;}
}
