package edu.bu.ec504.spr24.brain;

import edu.bu.ec504.spr24.brain.Board.Pos;
import edu.bu.ec504.spr24.sameGameTris.CircleColor;

/**
 * Does whatever a lazy brain does ...
 */
public class LazyBrain extends Brain {

	// FIELDS

	/**
	 * The Brain's recording of the current state of the board.
	 */
	private Board currBoard;

	// METHODS

	/**
	 * Constructs a Brain linked to a specified myGUI
	 */
	public LazyBrain() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	public String myName() {
		return "Lazy Brain";
	}

	@Override
	Pos nextMove() {
		// Initialize and set up the internal board state with the current position
		currBoard = new Board();
		for (int xx = 0; xx < myGUI.boardWidth(); xx++)
			for (int yy = 0; yy < myGUI.boardHeight(); yy++)
				currBoard.modify(xx, yy, myGUI.colorAt(xx, myGUI.boardHeight() - yy - 1));

		return chooseMove();
	}

	// PRIVATE METHODS

	/**
	 * Chooses the next move to make.
	 *
	 * @return the move chosen, in Board coordinates
	 */
	private Board.Pos chooseMove() {
		// greedy choice
		int max = 0;                  // the maximum number of points for the best position found
		Board.Pos bestPos = new Board.Pos(0, 0); // the best position found
		Board currStateCopy = new Board(currBoard);

		for (int xx = 0; xx < currBoard.columns(); xx++)
			for (int yy = 0; yy < currBoard.rows(xx); yy++) {
				if (currStateCopy.getAt(xx, yy) != CircleColor.NONE) {
					Board test = new Board(currStateCopy);

					// mark all other nodes in the region as "clear" (but does not delete anything)
					currStateCopy.clickNodeHelper(xx, yy, test.getAt(xx, yy));

					// try removing the region to see what is left over
					int count = test.clickNode(xx, yy);
					if (count > max) {
						// record a new best move
						max = count;
						bestPos = new Board.Pos(xx, yy);
					}

				}
			}

		// convert bestPos to GUI coordinates
		bestPos = new Board.Pos(bestPos.xx, myGUI.boardHeight() - 1 - bestPos.yy);

		// return the result to the GUI
		return bestPos;
	}
}

